package ru.t1consulting.vmironova.tm.api.service.model;

import ru.t1consulting.vmironova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}

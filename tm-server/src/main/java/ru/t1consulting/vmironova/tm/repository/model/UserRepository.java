package ru.t1consulting.vmironova.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.api.repository.model.IUserRepository;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) throws Exception {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) throws Exception {
        @NotNull final User user = create(login, password);
        user.setEmail((email == null) ? "" : email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) throws Exception {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) throws Exception {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Boolean isLoginExists(@NotNull final String login) throws Exception {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExists(@NotNull final String email) throws Exception {
        return findByEmail(email) != null;
    }

}
